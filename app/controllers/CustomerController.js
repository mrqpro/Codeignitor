/**
 * Created by mrqpro on 5/16/17.
 */

app.controller('CustomerController', function (dataFactory, $scope, $http) {

    $scope.data = [];
    $scope.pageNumber = 1;
    $scope.libraryTemp = {};
    $scope.totalItemsTemp = {};

    $scope.totalItems = 0;
    $scope.pageChanged = function (newPage) {
        getResultsPage(newPage);
    };

    getResultsPage(1); // Show the first page
    function getResultsPage(pageNumber) {
        if (!$.isEmptyObject($scope.libraryTemp)) {
            dataFactory.httpRequest('/customers?search=' + $scope.searchText + '&page=' + pageNumber).then(function (data) {
                $scope.data = data.data;
                $scope.totalItems = data.total;
                $scope.pageNumber = pageNumber;
            });
        } else {
            dataFactory.httpRequest('/customers?page=' + pageNumber).then(function (data) {
                $scope.data = data.data;
                $scope.totalItems = data.total;
                $scope.pageNumber = pageNumber;
            });
        }
    }

    // Search customer
    $scope.searchDB = function () {
        if ($scope.searchText.length >= 3) {
            if ($.isEmptyObject($scope.libraryTemp)) {
                $scope.libraryTemp = $scope.data;
                $scope.totalItemsTemp = $scope.totalItems;
                $scope.data = {};
            }
            getResultsPage(1);
        } else {
            if (!$.isEmptyObject($scope.libraryTemp)) {
                $scope.data = $scope.libraryTemp;
                $scope.totalItems = $scope.totalItemsTemp;
                $scope.libraryTemp = {};
            }
        }
    }

    // For creating new customer
    $scope.saveAdd = function () {
        dataFactory.httpRequest('customersCreate', 'POST', {}, $scope.form).then(function (data) {
            $scope.data.push(data);
            if (data.isError != 1) {
                $(".modal").modal("hide");
            }
        });
    }

    // For editing customer (get data)
    $scope.edit = function (id) {
        dataFactory.httpRequest('customersEdit/' + id).then(function (data) {
            $scope.form = data;
        });
    }

    // Editing customer (after click submit)
    $scope.saveEdit = function () {
        dataFactory.httpRequest('customersUpdate/' + $scope.form.id, 'PUT', {}, $scope.form).then(function (data) {
            $(".modal").modal("hide");
            $scope.data = apiModifyTable($scope.data, data.id, data);
        });
    }

    // Remove customer from database
    $scope.remove = function (item, index) {
        var result = confirm("Are you sure delete this item?");
        if (result) {
            dataFactory.httpRequest('customersDelete/' + item.id, 'DELETE').then(function (data) {
                $scope.data.splice(index, 1);
            });
        }
    }

});