-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2017 at 07:16 AM
-- Server version: 5.6.36
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codeignitor`
--
CREATE DATABASE IF NOT EXISTS `codeignitor` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `codeignitor`;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` char(50) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `fullname`, `email`, `created_date`, `updated_date`) VALUES
(3, 'Ulric', 'erat.eget.ipsum@orciadipiscing.edu', '2017-02-18', '2017-05-17'),
(4, 'Neville', 'malesuada.ut@eleifendnon.com', '2016-07-12', '2017-05-17'),
(5, 'Louis123', 'in.dolor@SeddictumProin.com', '2017-05-24', '2017-05-17'),
(7, 'Wesley123', 'Aliquam.adipiscing@eratsemper.com', '2016-05-24', '0000-00-00'),
(10, 'Merritt', 'massa@sedorci.co.uk', '2016-06-25', '0000-00-00'),
(11, 'Cairo', 'malesuada.vel@non.com', '2016-08-01', '0000-00-00'),
(12, 'Charles', 'pharetra@metus.com', '2017-09-17', '0000-00-00'),
(13, 'Colby', 'vulputate.eu.odio@bibendum.org', '2017-10-14', '0000-00-00'),
(14, 'Vladimir', 'libero@idnunc.co.uk', '2016-09-25', '0000-00-00'),
(15, 'Forrest', 'at@tinciduntorci.com', '2018-01-23', '0000-00-00'),
(16, 'Ira', 'arcu@egetipsumSuspendisse.co.uk', '2017-01-17', '0000-00-00'),
(17, 'Sylvester', 'Duis.ac@actellusSuspendisse.ca', '2016-12-07', '0000-00-00'),
(18, 'Hakeem', 'ipsum.Suspendisse.non@Phasellusat.edu', '2016-07-23', '0000-00-00'),
(19, 'Asher', 'turpis.Aliquam@Vestibulumaccumsan.com', '2016-06-12', '0000-00-00'),
(20, 'Kadeem', 'libero@dolor.co.uk', '2017-02-24', '0000-00-00'),
(21, 'Honorato', 'est@eudui.ca', '2018-04-19', '0000-00-00'),
(22, 'Griffith', 'lorem@AeneanmassaInteger.ca', '2017-05-12', '0000-00-00'),
(23, 'Nicholas', 'nisi@ametornare.edu', '2018-01-05', '0000-00-00'),
(24, 'Fritz', 'nisi@nonummy.edu', '2016-11-09', '0000-00-00'),
(25, 'Andrew', 'est.tempor.bibendum@Curabituregestas.ca', '2017-04-29', '0000-00-00'),
(26, 'Barrett', 'mauris.Suspendisse.aliquet@ligulaAenean.net', '2016-06-24', '0000-00-00'),
(27, 'Neville', 'Nunc.pulvinar@estacmattis.net', '2017-01-29', '0000-00-00'),
(28, 'Jasper', 'Suspendisse@etcommodoat.net', '2018-02-02', '0000-00-00'),
(29, 'Arsenio', 'nec.leo@Intincidunt.net', '2016-09-23', '0000-00-00'),
(30, 'Myles', 'id@etrutrumnon.com', '2018-05-13', '0000-00-00'),
(31, 'Holmes', 'velit@Pellentesquehabitant.edu', '2016-12-17', '0000-00-00'),
(32, 'Jesse', 'Nulla.facilisis@Cumsociis.co.uk', '2016-07-18', '0000-00-00'),
(33, 'Quang', 'quangvietvp@gmail.com', '0000-00-00', '0000-00-00'),
(43, 'Quang', 'viet.spiderman@gmail.com', '2017-05-17', '0000-00-00'),
(44, 'Quang', 'viet.spiderman@gmail.com1', '2017-05-17', '0000-00-00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
