<?php
/**
 * Created by PhpStorm.
 * User: mrqpro
 * Date: 5/16/17
 * Time: 11:18 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller
{
  public function index() {
    $this->load->database();

    // If get search condition
    if (!empty($this->input->get("search"))) {
      $this->db->like('fullname', $this->input->get("search"));
      $this->db->or_like('email', $this->input->get("search"));
    }

    // Get data, limit 5 records per page
    $this->db->limit(5, ($this->input->get("page", 1) - 1) * 5);
    $query = $this->db->get("customers");

    $data['data'] = $query->result();
    $data['total'] = $this->db->count_all("customers");

    echo json_encode($data);
  }

  // For click edit button
  public function edit($id) {
    $this->load->database();

    // Get return data
    $q = $this->db->get_where('customers', array('id' => $id));
    echo json_encode($q->row());
  }

  // After clicking submit
  public function update($id) {
    $this->load->database();

    // Get post data
    $_POST = json_decode(file_get_contents('php://input'), true);
    $insert = $this->input->post();
    $insert['updated_date'] = date('Y-m-d');

    // Update to database
    $this->db->where('id', $id);
    $this->db->update('customers', $insert);

    // Get return data
    $q = $this->db->get_where('customers', array('id' => $id));
    $return = $q->row();
    $return->jsMessage = 'Update customer successful';
    $return->jsTitle = 'Update customer';
    echo json_encode($return);
  }

  // Deleting record
  public function delete($id) {
    $this->load->database();
    $this->db->where('id', $id);
    $this->db->delete('customers');
    echo json_encode(['success' => true, 'jsTitle' => 'Delete customer', 'jsMessage' => 'Delete successful' ]);
  }

  // For saving customer
  public function store() {
    $this->load->database();

    // Get post data
    $_POST = json_decode(file_get_contents('php://input'), true);
    $insert = $this->input->post();

    // Checking data
    $email = $insert['email'];
    $error = false;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { // Check valid email
      $error = true;
      $errorMessage = 'Invalid email';
    } else {
      // Find by email
      $q = $this->db->get_where('customers', array('email' => $insert['email']));
      $return = $q->row();
      if (count($return) > 0) {
        $error = true;
        $errorMessage = 'Email existsed';
      }
    }

    if ($error) { // If has error
      echo json_encode(['jsMessage' => $errorMessage, 'jsTitle' => 'Error', 'isError' => 1]);
    } else { // If data is valid
      $insert['created_date'] = date('Y-m-d');
      $this->db->insert('customers', $insert);

      // Get return data
      $id = $this->db->insert_id();
      $q = $this->db->get_where('customers', array('id' => $id));
      $return = $q->row();
      $return->jsMessage = 'Create customer successful';
      $return->jsTitle = 'Create customer';
      echo json_encode($return);
    }
  }
}